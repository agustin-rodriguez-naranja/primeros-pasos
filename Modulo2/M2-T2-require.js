const fs = require("fs");

let tablaDelCinco = "";

for (let i = 1; i <= 10; i++) {
  tablaDelCinco += `5 x ${i} = ${5 * i}\n`;
}

fs.writeFile("tabla-5.txt", tablaDelCinco, (error) => {
  if (error) {
    console.log(`Ocurrió un error: ${error}`);
  } else {
    console.log("Archivo generado correctamente.");
  }
});
