const fs = require("fs");

const crearArchivo = () => {
  let tablaDelCinco = "";

  for (let i = 1; i <= 10; i++) {
    tablaDelCinco += `5 x ${i} = ${5 * i}\n`;
  }

  return new Promise(function (resolve, reject) {
    fs.writeFile("tabla-5.txt", tablaDelCinco, (error) => {
      if (error) {
        reject(`Ocurrió un error: ${error}`);
      } else {
        resolve("Archivo generado correctamente.");
      }
    });
  });
};

module.exports = crearArchivo;
