const crearArchivo = require('./crearArchivo')
const argv = require('./config/yargs')

const base = argv.base
const debeListar = argv.listar

crearArchivo(base, debeListar)
  .then((res) => console.log(res))
  .catch((err) => console.log(err))
