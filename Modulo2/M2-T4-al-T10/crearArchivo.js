const fs = require('fs')
const colors = require('colors')

const crearArchivo = (base = 5, debeListar) => {
  return new Promise(function (resolve, reject) {
    if (typeof base !== 'number') {
      reject(new Error('Se requiere un número para obtener su tabla'))
      return
    }

    let tablaArchivo = ''
    let tablaConsola =
      `==============================\n         TABLA DEL ${base}\n==============================\n`
        .rainbow

    for (let i = 1; i <= 10; i++) {
      tablaArchivo += `${base} x ${i} = ${base * i}\n`
      tablaConsola += `${base} ${'x'.green} ${i} ${'='.green} ${base * i}\n`
    }

    fs.writeFile(`./salida/tabla-${base}.txt`, tablaArchivo, (error) => {
      if (error) {
        reject(new Error(`Ocurrió un error: ${error}`))
      } else {
        if (debeListar) {
          resolve(tablaConsola)
        } else {
          resolve('Tabla generada correctamente.')
        }
      }
    })
  })
}

module.exports = crearArchivo
