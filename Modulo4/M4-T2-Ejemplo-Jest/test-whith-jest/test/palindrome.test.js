const { palindrome, average } = require('../utils/utility-for-testing')
describe('metodos de Util', () => {
  
  test('palindrome of mindhub', () => {
    const result = palindrome('mindhub')
    expect(result).toBe('buhdnim')
  })

  test('palindrome of empty string', () => {
    const result = palindrome('')
    expect(result).toBe('')
  })

  test('palindrome of undefined', () => {
    const result = palindrome()
    expect(result).toBeUndefined()
  })

  test('average of three numbers', () => {
    const result = average([1,5,3])
    expect(result).toBe(3);
  })

  test('average of empty array', () => {
    const result = average([])
    expect(result).toBeUndefined();
  })

  test('average of no array', () => {
    const result = average({})
    expect(result).toBeUndefined();
  })

})
