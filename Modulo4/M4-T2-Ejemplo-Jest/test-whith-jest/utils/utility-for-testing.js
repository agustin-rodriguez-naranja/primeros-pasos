const palindrome = (string) => {
	if (typeof string === 'undefined') return
  return string.split('').reverse().join('')
}

const average = array => {
  let sum = 0

  if (!Array.isArray(array)) {
    console.log('Se requiere un array de numeros')
    return;
  }

  if (array.length === 0) {
    console.log('Se requiere al menos un numero')
    return;
  }

  array.forEach(num => { sum += num })
  return sum / array.length
}

module.exports = {
  palindrome,
  average
}