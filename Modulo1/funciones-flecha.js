var suma = (a, b) => {
  if (typeof a !== "number" || typeof b !== "number") {
    console.log("Los operandos deben ser números.");
    return;
  }

  return a + b;
};

var resta = (a, b) => {
  if (typeof a !== "number" || typeof b !== "number") {
    console.log("Los operandos deben ser números.");
    return;
  }

  return a - b;
};

var multiplicacion = (a, b) => {
  if (typeof a !== "number" || typeof b !== "number") {
    console.log("Los operandos deben ser números.");
    return;
  }

  return a * b;
};

var division = (a, b) => {
  if (typeof a !== "number" || typeof b !== "number" || b === 0) {
    console.log(
      "Los operandos deben ser números y el divisor distinto de cero."
    );
    return;
  }

  return a / b;
};

const a = 10;
const b = 1;

console.log(`La suma entre ${a} y ${b} da ${suma(a, b)}`);
console.log(`La resta entre ${a} y ${b} da ${resta(a, b)}`);
console.log(`La multiplicación entre ${a} y ${b} da ${multiplicacion(a, b)}`);
console.log(`La división entre ${a} y ${b} da ${division(a, b)}`);
