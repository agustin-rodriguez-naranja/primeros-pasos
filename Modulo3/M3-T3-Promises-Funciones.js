const informarResultado = (resultado, error = false) => {
  if (error) {
    console.log(`Error: ${resultado}`);
  } else {
    console.log(`Mensaje: ${resultado}`);
  }
}

const contratarJugador = new Promise( (resolve, reject) => {
  const contratado = true;

  if(contratado) {
      resolve('El jugador se incorpora al equipo')
  } else {
      reject('El jugador y el equipo no llegaron a un acuerdo')
  }
})

contratarJugador.then(res => informarResultado(res)).catch(err => informarResultado(err, true));