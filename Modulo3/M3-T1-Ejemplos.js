//Comienza Ejemplo 1: Sin Callbacks


const lenguajesEjemplo1 = ['Node', 'React', 'Java', 'Python', 'Ruby on Rails'];

function mostrarLenguajesEjemplo1() {
  setTimeout(() => {
    console.log("Mostrando ejemplo Sin Callback");
    lenguajesEjemplo1.forEach(lenguaje => console.log(lenguaje));
  }, 1000);
}

mostrarLenguajesEjemplo1();

//Comienza Ejemplo 2: Con Callbacks


const lenguajesEjemplo2 = ['Node', 'React', 'Java', 'Python', 'Ruby on Rails'];

function mostrarLenguajesEjemplo2() {
  console.log("-----------------\nMostrando ejemplo Con Callback");
  lenguajesEjemplo2.forEach(lenguaje => console.log(lenguaje));
}

function nuevoLenguaje(lenguaje, callback) {
  setTimeout(() => {
    lenguajesEjemplo2.push(lenguaje);
    callback();
  }, 2000);
}
nuevoLenguaje('Angular', mostrarLenguajesEjemplo2);