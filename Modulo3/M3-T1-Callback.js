const elementos = [];

const mostrarElementos = () => {
  elementos.forEach(el => console.log(el));
}

const agregarElemento = (nuevoElemento, callback) => {
  elementos.push(nuevoElemento);
  callback();
}

const iniciar = () => {
  agregarElemento("Juan", mostrarElementos);
  agregarElemento("Maria", mostrarElementos);
  agregarElemento("Ana", mostrarElementos);
  agregarElemento("Matias", mostrarElementos);
  agregarElemento("Carlos", mostrarElementos);
}

iniciar();