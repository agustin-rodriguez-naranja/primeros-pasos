// const pausa = () => {
//   return new Promise((resolve, reject) => {
//     const readline = require('readline')

//     const rl = readline.createInterface({
//       input: process.stdin,
//       output: process.stdout
//     })

//     rl.question(`Presione ${'ENTER'.blue} para continuar\n`, () => {
//       console.log('Continuando')
//       resolve()

//       rl.close()
//     })
//   })
// }

const pausa = async () => {
  try {
    const inquirer = require('inquirer')

    const answer = await inquirer.prompt({
      type: 'input',
      name: 'enter',
      message: `Presione ${'ENTER'.blue} para continuar`
    })

    return answer.enter
  } catch (error) {
    return error
  }
}

const mostrarMenu = () => {
  return new Promise((resolve, reject) => {
    console.log('==============================\n    Seleccione una opción\n=============================='.rainbow)

    console.log('¿Qué desea hacer?'.blue)
    console.log(`${'1.'.blue} Crear tarea\n${'2.'.blue} Listar tareas\n${'3.'.blue} Listar tareas completadas\n${'4.'.blue} Listar tareas pendientes\n${'5.'.blue} Completar tarea(s)\n${'6.'.blue} Borrar tarea\n${'0.'.blue} Salir\n`)

    const readline = require('readline')

    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    rl.question('Seleccione una opción: ', (answer) => {
      // console.log({opt: answer})
      if (isNaN(answer) || answer < 0 || answer > 6) {
        reject('La opción debe ser un número entre 0 y 6')
      } else {
        resolve(answer)
      }

      rl.close()
    })
  })
}

module.exports = {
  pausa,
  mostrarMenu,
}
