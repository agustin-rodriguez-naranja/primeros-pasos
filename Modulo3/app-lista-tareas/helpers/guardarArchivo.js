const fs = require('fs')

const guardarDB = (tarea) => {
  return new Promise(function (resolve, reject) {
    fs.writeFile('./db/lista-de-tareas.json', JSON.stringify(tarea), (error) => {
      if (error) {
        reject(`Ocurrió un error: ${error}`)
      } else {
        resolve('Archivo generado correctamente.')
      }
    })
  })
}

const leerDB = () => {
  return new Promise(function (resolve, reject) {
    if (!fs.existsSync('./db/lista-de-tareas.json')) {
      resolve([])
    } else {
      fs.readFile('./db/lista-de-tareas.json', 'utf8', (err, data) => {
        if (err) {
          reject('Error al leer del archivo de tareas')
        } else {
          resolve(JSON.parse(data))
        }
      })
    }
  })
}

module.exports = {
  guardarDB,
  leerDB
}
