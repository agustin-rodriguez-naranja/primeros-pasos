const inquirer = require('inquirer')

const mostrarMenu = async () => {
  try {
    const answer = await inquirer.prompt({
      type: 'list',
      name: 'opt',
      message: 'Seleccione una opción',
      choices: [
        {
          name: '1. Crear tarea',
          value: 1,
        },
        {
          name: '2. Listar tareas',
          value: 2,
        },
        {
          name: '3. Listar tareas completadas',
          value: 3,
        },
        {
          name: '4. Listar tareas pendientes',
          value: 4,
        },
        {
          name: '5. Completar tarea(s)',
          value: 5,
        },
        {
          name: '6. Borrar tarea',
          value: 6,
        },
        {
          name: '0. Salir',
          value: 0,
        },
      ]
    })

    return answer.opt
  } catch (error) {
    return error
  }
}

const leerInput = async (message) => {
  try {
    const { desc } = await inquirer.prompt({
      type: 'input',
      name: 'desc',
      message: 'Descripción de la tarea: ',
      validate(value) {
        if (value.length === 0) {
          return 'Por favor, ingrese un valor'
        }
        return true
      }
    })

    return desc
  } catch (error) {
    return error
  }
}

const listadoTareasBorrar = async (tareas) => {
  try {
    const choices = tareas.map(({id, desc}, i) => {
      const numeroTarea = i + 1

      return {
        name: `${numeroTarea.toString().blue} ${desc}`,
        value: id
      }
    })
    
    choices.unshift({
      name: `${'0'.blue} Cancelar`,
      value: 0
    })

    const { del } = await inquirer.prompt({
      type: 'list',
      name: 'del',
      message: 'Seleccione una opción',
      choices
    })

    return del
  } catch (error) {
    return error
  }
}

const confirmar = async (message) => {
  try {
    const question = {
      type: 'confirm',
      name: 'ok',
      message
    }

    const { ok } = await inquirer.prompt(question)

    return ok
  } catch (error) {
    return error
  }
}

const mostrarListadoChecklist = async (tareas) => {
  try {
    const choices = tareas.map(({id, desc, completadoEn}, i) => {
      const numeroTarea = i + 1

      return {
        name: `${numeroTarea.toString().blue} ${desc}`,
        value: id,
        checked: completadoEn ? true : false
      }
    })
    
    const { ids } = await inquirer.prompt({
      type: 'checkbox',
      name: 'ids',
      message: 'Selecciones',
      choices
    })

    return ids
  } catch (error) {
    return error
  }
}

module.exports = {
  mostrarMenu,
  leerInput,
  listadoTareasBorrar,
  confirmar,
  mostrarListadoChecklist
}
