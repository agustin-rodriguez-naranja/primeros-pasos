const Tarea = require('./tarea')

class Tareas {
  listado = null

  constructor() {
    this.listado = {}
  }

  cargarTareasFromArray(tareas = []) {
    tareas.forEach(tarea => this.listado[tarea.id] = tarea)
  }

  get listadoArr() {
    const lista = []
    const keys = Object.keys(this.listado)

    keys.forEach(key => lista.push(this.listado[key]))

    return lista
  }

  listadoCompleto() {
    if (this.listadoArr.length === 0) {
      console.log('Aún no se han cargado tareas')
    } else {
      this.listadoArr.forEach((tarea, i) => {
        const numeroTarea = i + 1
        const tareaFormateada = `${numeroTarea.toString().blue} ${tarea.desc} :: ${tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red}`
        console.log(tareaFormateada)
      })
    }
  }

  listarPendientesCompletadas(completadas = true) {
    if (completadas) {
      const tareasCompletadas = this.listadoArr.filter(tarea => tarea.completadoEn !== null)

      if (tareasCompletadas.length === 0) {
        console.log('No hay tareas completadas')
        return;
      }

      tareasCompletadas.forEach((tarea, i) => {
        const numeroTarea = i + 1
        const tareaFormateada = `${numeroTarea.toString().green} ${tarea.desc}` //se usa .toString() para evitar conflictos con .green

        console.log(tareaFormateada)
      })    
    } else {
      const tareasPendientes = this.listadoArr.filter(tarea => tarea.completadoEn === null)

      if (tareasPendientes.length === 0) {
        console.log('No hay tareas completadas')
        return;
      }

      tareasPendientes.forEach((tarea, i) => {
        const numeroTarea = i + 1
        const tareaFormateada = `${numeroTarea.toString().red} ${tarea.desc}` //se usa .toString() para evitar conflictos con .green

        console.log(tareaFormateada)
      })
    }
  }

  crearTarea(desc) {
    const tarea = new Tarea(desc)

    this.listado[tarea.id] = tarea;
  }

  borrarTarea(id) {
    if (this.listado[id]) {
      delete this.listado[id]
    } else {
      console.log("No existe tarea con el id proporcionado")
    }
  }

  toggleCompletadas(ids = []) {
    ids.forEach(id => {
      const tarea = this.listado[id]

      if (!tarea.completadoEn) {
        tarea.completadoEn = new Date().toISOString()
      }
    })

    this.listadoArr.forEach(({id}) => {
      if (!ids.includes(id)) {
        this.listado[id].completadoEn = null
      }
    })
  }
}

module.exports = Tareas