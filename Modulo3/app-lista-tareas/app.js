const colors = require('colors')
const { pausa } = require('./helpers/mensajes')
const { mostrarMenu, leerInput, listadoTareasBorrar, confirmar, mostrarListadoChecklist } = require('./helpers/inquirer')
const Tarea = require('./models/tarea')
const Tareas = require('./models/tareas')
const { guardarDB, leerDB } = require('./helpers/guardarArchivo')

console.clear()

const main = async () => {
  try {
    console.log('Hola mundo')

    let opt = ''

    const tareas = new Tareas()

    const listaDeTareas = await leerDB()
    tareas.cargarTareasFromArray(listaDeTareas)

    do {
      opt = await mostrarMenu()
      // console.log({opt})
      // await pausa()

      switch (opt) {
        case 1:
          const desc = await leerInput()
          tareas.crearTarea(desc)
          console.log(desc)
          break
  
        case 2:
          tareas.listadoCompleto()
          break

        case 3:
          tareas.listarPendientesCompletadas(true)
          break

        case 4:
          tareas.listarPendientesCompletadas(false)
          break

        case 5:
          const ids = await mostrarListadoChecklist(tareas.listadoArr)
          tareas.toggleCompletadas(ids)
          break

        case 6:
          const id = await listadoTareasBorrar(tareas.listadoArr)

          if (id !== 0) {
            const ok = await confirmar('¿Está seguro?')

            if (ok) {
              tareas.borrarTarea(id)
              console.log('Tarea borrada')
            }
          }
          break

        default:
          break
      }

      await guardarDB(tareas.listadoArr)

      await pausa()
    } while (opt !== 0)
  } catch (error) {
    console.log(`Error: ${error}`)
  } 
}

main()
