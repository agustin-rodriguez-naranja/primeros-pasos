module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true
  },
  extends: [
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 12
  },
  rules: {
    'comma-dangle': ['error', 'only-multiline'],
    'no-unused-vars': 'off',
    'object-curly-spacing': 'off',
    'no-trailing-spaces': 'off',
    'prefer-promise-reject-errors': 'off',
    'space-before-function-paren': 'off',
    'no-case-declarations': 'off',
    'no-unneeded-ternary': 'off'
  }
}
